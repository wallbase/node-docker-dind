FROM node:16.19.1-alpine3.17
MAINTAINER  wangkun23 <845885222@qq.com>
# Default to UTF-8 file.encoding
ENV LANG C.UTF-8
#RUN echo https://mirrors.aliyun.com/alpine/v3.11/main > /etc/apk/repositories && \
#    echo https://mirrors.aliyun.com/alpine/v3.11/community >> /etc/apk/repositories
RUN apk update && apk upgrade
# RUN apk add libstdc++
RUN apk add openrc python --no-cache
# install docker and git
RUN apk add docker git make gcc g++
# To start the Docker daemon at boot, run:
RUN rc-update add docker boot
